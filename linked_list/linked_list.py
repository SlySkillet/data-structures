# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_list.
#
# Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_19.py.

class LinkedListNode:
    def __init__(self, value):
        self.value = value
        self.link = None

class LinkedList:
    def __init__(self):
        self.head = None
        self.tail = None

    def insert(self, value, index=None):
        node = LinkedListNode(value)
        if self.head == None:
            self.head = node
            self.tail = node
